from pygame.math import Vector2 as vec
#Game stages
START = 'intro'
PLAYING = 'playing'
GAME_OVER = 'game over'
GAME_WIN = 'game_win'

# screeing setting
MAZE_WIDTH, MAZE_HEIGHT = 532, 589
MARGIN_TOP = 40
MARGIN_LEFT = 50
MARGIN_RIGHT = 50
MARGIN_BOTTOM = 40
WIDTH = MAZE_WIDTH + MARGIN_LEFT + MARGIN_RIGHT
HEIGHT = MAZE_HEIGHT + MARGIN_TOP + MARGIN_BOTTOM
FPS = 60
ROWS = 19
COLS = 21

#color setting
BLACK = (0,0,0)
WHITE = (255,255,255)
ORANGE = (170, 132, 58)
BLUE = (43,108,255)
GREY = (107, 107, 107)
RED = (255,0,0)
GREEN = (0,255,0)
PINK = (255,56,152)
COINS_COLOR = (167,179,34)

#intro setting
START_TEXT_SIZE = 16
START_FONT = 'arial black'

#player setting
PLAYER_START_POS = vec(13, 15)
PLAYER_COLOR = 'yellow'
#enemies setting
