from pygame import Vector2 as vec
import pygame
from setting import *

class Player:

    def __init__(self, app, pos):
        self.app = app
        self.grid_pos = pos
        self.pix_pos = vec(self.maze_x_pix_pos(self.grid_pos.x), self.maze_y_pix_pos(self.grid_pos.y))
        self.direction = vec(-1, 0)
        self.stored_direction = None
        self.score = 0
        self.speed = 2
        self.lifes = 3

    def maze_x_pix_pos(self, grid_x):
        return self.app.maze_x(grid_x * self.app.cell_width)

    def maze_y_pix_pos(self, grid_y):
        return self.app.maze_y(grid_y * self.app.cell_height)

    def update(self):
        if self.can_move():
            self.pix_pos += self.direction * self.speed

        if self.time_to_move():
            self.direction = self.stored_direction if self.stored_direction is not None else self.direction

        if self.on_coin():
            self.eat_coin()

        if self.on_fruit():
            self.eat_fruit()

        for e in self.app.enemies:
            if self.grid_pos == e.grid_pos:
                self.pix_pos = e.pix_pos

        '''
        if self.direction == vec(-1, 0) or self.direction == vec(1, 0):
            print(self.grid_pos.y * self.app.cell_height + MARGIN_TOP, self.pix_pos.y)
            self.pix_pos.y = self.grid_pos.y * self.app.cell_height + MARGIN_TOP
        if self.direction == vec(0, -1) or self.direction == vec(0, 1):
            self.pix_pos.x = self.grid_pos.x * self.app.cell_width + MARGIN_LEFT

        if int(self.pix_pos.x + radio) % self.app.cell_width == 0:
            #print('x is in line')
            if self.direction == vec(-1,0) or self.direction == vec(1,0):
                self.direction = self.stored_direction if self.stored_direction is not None else self.direction

        if int(self.pix_pos.y + radio) % self.app.cell_height == 0:
            #print('x is in line')
            if self.direction == vec(0,-1) or self.direction == vec(0,1):
                self.direction = self.stored_direction if self.stored_direction is not None else self.direction

        '''

    def draw(self):
        pix_pos = (self.pix_pos[0] + self.app.cell_width // 2, self.pix_pos[1] + self.app.cell_height // 2)
        pygame.draw.circle(self.app.screen, PLAYER_COLOR, pix_pos, self.app.cell_width // 2 - 3)
        #pygame.draw.rect(self.app.screen, RED,
        #                 (self.maze_x_pix_pos(self.grid_pos[0]), self.maze_y_pix_pos(self.grid_pos[1]),
        #                 self.app.cell_width, self.app.cell_height), 1)
        for life in range(self.lifes - 1):
            pygame.draw.circle(self.app.screen, PLAYER_COLOR, (self.app.maze_x((2 + life) * self.app.cell_width), self.app.maze_y((ROWS + 2) * self.app.cell_height) + self.app.cell_height//1.2), self.app.cell_width // 2 - 3)

    def move(self, direction):
        self.stored_direction = direction

    def time_to_move(self):
        if self.direction == vec(-1, 0) or self.direction == vec(1, 0):
            offset_x = -8
            if int(self.pix_pos.x + offset_x) % int(self.app.cell_width // 2) == 0 and int(self.pix_pos.x + offset_x) % (self.app.cell_width) != 0:
                self.grid_pos = vec(self.pix_pos[0] // self.app.cell_width - 1, self.pix_pos[1] // self.app.cell_height - 1)
                if self.grid_pos == vec(18,9) and self.direction == vec(1, 0):
                    self.grid_pos = vec(0, 9)
                    self.pix_pos = vec(self.maze_x_pix_pos(self.grid_pos.x), self.maze_y_pix_pos(self.grid_pos.y))
                elif self.grid_pos == vec(0,9) and self.direction == vec(-1, 0):
                    self.grid_pos = vec(18, 9)
                    self.pix_pos = vec(self.maze_x_pix_pos(self.grid_pos.x), self.maze_y_pix_pos(self.grid_pos.y))

                return not self.hit_wall()

        if self.direction == vec(0, -1) or self.direction == vec(0, 1):
            offset_y = 2
            if int(self.pix_pos.y + offset_y) % (self.app.cell_height // 2) == 0 and int(self.pix_pos.y + offset_y) % (self.app.cell_height) != 0:
                self.grid_pos = vec(self.pix_pos[0] // self.app.cell_width - 1, self.pix_pos[1] // self.app.cell_height - 1)
                return not self.hit_wall()

        return False

    def hit_wall(self):
        if self.stored_direction is not None:
            if self.grid_pos + self.stored_direction in self.app.walls:
                return True

        return False

    def can_move(self):
        #print("grid: ", self.grid_pos)
        #print("grid: ", self.grid_pos + self.direction)
        if self.grid_pos + self.direction not in self.app.walls:
            for e in self.app.enemies:
                if self.grid_pos == e.grid_pos:
                    return False
            return True

        return False

    def on_coin(self):
        return self.grid_pos in self.app.coins

    def on_fruit(self):
        return self.grid_pos in self.app.fruits

    def eat_coin(self):
        self.app.coins.remove(self.grid_pos)
        self.score += 10

    def eat_fruit(self):
        self.app.fruits.remove(self.grid_pos)
        self.score += 100

    def remove_life(self):
        self.lifes -= 1
        self.grid_pos = PLAYER_START_POS
        self.pix_pos = vec(self.maze_x_pix_pos(self.grid_pos.x), self.maze_y_pix_pos(self.grid_pos.y))
        self.direction = vec(-1,0)
        self.stored_direction = None

    def restart(self, game_over=False):
        if game_over:
            self.score = 0
            self.lifes = 3
        self.grid_pos = PLAYER_START_POS
        self.pix_pos = vec(self.maze_x_pix_pos(self.grid_pos.x), self.maze_y_pix_pos(self.grid_pos.y))
