from pygame import Vector2 as vec
import pygame
from setting import *
import random

change = 0
keep = 0
class Phantom:

    def __init__(self, app, character, starting_grid_pos, color, speed=1):
        self.app = app
        self.starting_grid_pos = starting_grid_pos
        self.grid_pos = starting_grid_pos
        self.pix_pos = vec(self.maze_x_pix_pos(self.grid_pos.x), self.maze_y_pix_pos(self.grid_pos.y))
        self.character = character
        self.direction = vec(1,0)
        self.color = color
        self.speed = speed


    def maze_x_pix_pos(self, grid_x):
        return self.app.maze_x(grid_x * self.app.cell_width)

    def maze_y_pix_pos(self, grid_y):
        return self.app.maze_y(grid_y * self.app.cell_height)

    def update(self):
        if self.can_move():
            self.pix_pos += self.direction * self.speed
        else:
            self.direction = self.get_random_dir()

        if self.time_to_move():
            if self.character == 'random':
                self.direction = self.get_random_dir()
            if self.character == 'hunter':
                self.direction = self.get_hunter_dir()
            if self.character == 'funky':
                self.direction = self.get_funcky_dir()


        if self.grid_pos == self.app.player.grid_pos:
            self.pix_pos = self.app.player.pix_pos


    def draw(self):
        pix_pos = (self.pix_pos[0] + self.app.cell_width // 2, self.pix_pos[1] + self.app.cell_height // 2)
        pygame.draw.circle(self.app.screen, self.color, pix_pos, self.app.cell_width // 2 - 3)
        #pygame.draw.rect(self.app.screen, RED,
        #                 (self.maze_x_pix_pos(self.grid_pos[0]), self.maze_y_pix_pos(self.grid_pos[1]),
        #                 self.app.cell_width, self.app.cell_height), 1)

    def time_to_move(self):
        if self.direction == vec(-1, 0) or self.direction == vec(1, 0):
            offset_x = -8
            if int(self.pix_pos.x + offset_x) % int(self.app.cell_width // 2) == 0 and int(self.pix_pos.x + offset_x) % (self.app.cell_width) != 0:
                self.grid_pos = vec(self.pix_pos[0] // self.app.cell_width - 1, self.pix_pos[1] // self.app.cell_height - 1)
                if self.grid_pos == vec(18,9) and self.direction == vec(1, 0):
                    self.grid_pos = vec(0, 9)
                    self.pix_pos = vec(self.maze_x_pix_pos(self.grid_pos.x), self.maze_y_pix_pos(self.grid_pos.y))
                elif self.grid_pos == vec(0,9) and self.direction == vec(-1, 0):
                    self.grid_pos = vec(18, 9)
                    self.pix_pos = vec(self.maze_x_pix_pos(self.grid_pos.x), self.maze_y_pix_pos(self.grid_pos.y))

                return not self.hit_wall()

        if self.direction == vec(0, -1) or self.direction == vec(0, 1):
            offset_y = 2
            if int(self.pix_pos.y + offset_y) % (self.app.cell_height // 2) == 0 and int(self.pix_pos.y + offset_y) % (self.app.cell_height) != 0:
                self.grid_pos = vec(self.pix_pos[0] // self.app.cell_width - 1, self.pix_pos[1] // self.app.cell_height - 1)
                return not self.hit_wall()

        return self.direction == vec(0,0)

    def can_move(self):
        if self.grid_pos + self.direction not in self.app.walls and self.grid_pos != self.app.player.grid_pos:
            return True

        return False

    def hit_wall(self):
        if self.direction is not None:
            if self.grid_pos + self.direction in self.app.walls:
                return True

        return False

    def get_random_dir(self):
        global keep, change
        mov_possibilities = []
        if self.grid_pos + (-1, 0) not in self.app.walls:
            mov_possibilities.append(vec(-1,0))
        if self.grid_pos + (1, 0) not in self.app.walls:
            mov_possibilities.append(vec(1,0))
        if self.grid_pos + (0, -1) not in self.app.walls:
            mov_possibilities.append(vec(0,-1))
        if self.grid_pos + (0, 1) not in self.app.walls:
            mov_possibilities.append(vec(0,1))

        #Code to make more likely to keep the current direction and avoid turning back when possible
        if self.direction in mov_possibilities:
            if random.random()<=0.75 or len(mov_possibilities) == 2:
                keep+=1
                return self.direction
            else:
                if len(mov_possibilities) > 1:
                    mov_possibilities.remove(self.direction)
                change += 1

        #Code to avoid  turning back when possible
        if len(mov_possibilities) > 1 and self.opposite_dir(self.direction) in mov_possibilities:
                mov_possibilities.remove(self.opposite_dir(self.direction))

        #print(keep/(keep+change))
        return random.choice(mov_possibilities)

    def opposite_dir(self, v):
        return vec(-v[0], -v[1])

    def bfs(self, target):
        queue = [self.grid_pos]
        visited = []
        path = []
        while len(queue) > 0:
            pos = queue.pop(0)
            #print(pos)
            if pos not in visited:
                visited.append(pos)
                if pos == target:
                    break

                if pos== vec(0, 9):
                    next_positions = [vec(18, 9), vec(1, 9)]
                elif pos == vec(18, 9):
                    next_positions = [vec(1, 9), vec(17, 9)]
                else:
                    next_positions = [pos + vec(-1,0), pos + vec(1,0), pos + vec(0,-1), pos + vec(0,1)]
                for next_pos in next_positions:
                    if next_pos not in self.app.walls and next_pos not in visited:
                        queue.append(next_pos)
                        path.append((pos, next_pos))

        return path

    def get_hunter_dir(self):
        bfs = self.bfs(self.app.player.grid_pos)
        #print(bfs)
        #print("\n")
        path = [vec(0,0)]
        pos = self.app.player.grid_pos
        while pos != self.grid_pos:
            for f,t in bfs:
                if pos == t:
                    path.append(t - f)
                    pos = f
                    break

        #print("Next " ,path)
        return path[-1]

    def get_funcky_dir(self):
        # split the maze in 4 cuadrants, state where the player is and chose the opposite
        #
        if self.app.player.grid_pos.x < 19//2:
            if self.app.player.grid_pos.y < 21//2:
                opp_dir = vec(ROWS-2, COLS-2)
            else:
                opp_dir = vec(ROWS-2, 1)
        else:
            if self.app.player.grid_pos.y < 21//2:
                opp_dir = vec(1, COLS-2)
            else:
                opp_dir = vec(1, 1)

        #print(opp_dir)

        bfs = self.bfs(opp_dir)
        #print("end bfs")

        path = [vec(0,0)]
        pos = opp_dir
        while pos != self.grid_pos:
            for f,t in bfs:
                if pos == t:
                    path.append(t - f)
                    pos = f
                    break

        #print("Next " ,path)
        return path[-1]




    def restart(self):
        self.grid_pos = self.starting_grid_pos
        self.pix_pos = vec(self.maze_x_pix_pos(self.grid_pos.x), self.maze_y_pix_pos(self.grid_pos.y))


