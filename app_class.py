import pygame
import sys
from player_class import Player
from setting import *
from phantom_class import Phantom
from time import sleep

pygame.init()
vec = pygame.math.Vector2

class App:

    def __init__(self):
        self.screen = pygame.display.set_mode((WIDTH, HEIGHT))
        self.clock = pygame.time.Clock()
        self.running = True
        self.state = START
        self.background = self.load_background()
        self.cell_width = MAZE_WIDTH // ROWS
        self.cell_height = MAZE_HEIGHT // COLS
        self.player = Player(self, PLAYER_START_POS)
        self.walls = []
        self.coins = []
        self.enemies = []
        self.fruit_clock = 500
        self.fruits = []
        self.load_file()


    def run(self):
        while self.running:
            if self.state == START:
                self.start_events()
                self.start_update()
                self.start_draw()
            elif self.state == PLAYING:
                self.playing_events()
                self.playing_update()
                self.playing_draw()
            elif self.state == GAME_OVER:
                self.game_over_events()
                self.game_over_update()
                self.game_over_draw()
            elif self.state == GAME_WIN:
                self.game_win_events()
                self.game_win_update()
                self.game_win_draw()
            else:
                pass
            self.clock.tick(FPS)

        pygame.quit()
        sys.exit()

    def restart_game(self, game_over=True):
        print("HOLA")
        self.player.restart(game_over)
        self.walls = []
        self.coins = []
        self.enemies = []
        self.load_file()


    ###########################################################
    ##################### Help functions #####################
    ###########################################################

    def draw_text(self, text, screen, pos, size, color, font_name, centered=False):
        font = pygame.font.SysFont(font_name, size)
        r_text = font.render(text, False, color)
        text_size = r_text.get_size()
        if centered:
            pos = (pos[0]-(text_size[0]//2), pos[1]-(text_size[1]//2))
        screen.blit(r_text, pos)

    def load_background(self):
        background = pygame.image.load('resources/maze.png')
        background = pygame.transform.scale(background, (MAZE_WIDTH, MAZE_HEIGHT))
        return background

    def draw_grid(self):
        for x in range(MAZE_WIDTH//self.cell_width + 1):
            pygame.draw.line(self.screen, WHITE, (self.maze_x(x * self.cell_width), MARGIN_TOP), (self.maze_x(x * self.cell_width), MAZE_HEIGHT + MARGIN_TOP))
        for y in range(MAZE_HEIGHT//self.cell_height + 1):
            pygame.draw.line(self.screen, WHITE, (MARGIN_LEFT, self.maze_y(y*self.cell_height)), (MAZE_WIDTH + MARGIN_LEFT, self.maze_y(y * self.cell_height)))
        #for w_x, w_y in self.walls:
        #    pygame.draw.rect(self.screen, RED, (self.maze_x(w_x * self.cell_width), self.maze_y(w_y * self.cell_height),  self.cell_width, self.cell_height))

    def maze_x(self, x):
        return x + MARGIN_LEFT

    def maze_y(self, y):
        return y + MARGIN_TOP

    def load_file(self):
        with open('resources/walls.txt','r') as f:
            for j,l in enumerate(f):
                for i,c in enumerate(l):
                    if c == 'F':
                        self.fruits.append((int(i),int(j)))
                    elif c == '1':
                        self.walls.append((int(i),int(j)))
                    elif c == 'C':
                        self.coins.append((int(i),int(j)))
                    elif c == '2':
                        self.enemies.append(Phantom(self, 'random', vec(int(i),int(j)), RED, 2))
                    elif c == '3':
                        self.enemies.append(Phantom(self, 'hunter', vec(int(i), int(j)), BLUE, 1))
                    elif c == '4':
                        self.enemies.append(Phantom(self, 'hunter', vec(int(i), int(j)), GREEN, 2))
                    elif c == '5':
                        self.enemies.append(Phantom(self, 'funky', vec(int(i), int(j)), PINK, 1))

        #print(self.walls)
        #print(self.coins)
        #print(self.enemies)

    ###########################################################
    ##################### Intro functions #####################
    ###########################################################

    def start_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False
            if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                self.restart_game()
                self.state = PLAYING


    def start_update(self):
        pass

    def start_draw(self):
        self.screen.fill(BLACK)
        self.draw_text('HIGH SCORE', self.screen, (3, 0), START_TEXT_SIZE, WHITE, START_FONT)
        self.draw_text('PUSH SPACE BAR', self.screen, (WIDTH//2, HEIGHT//2), START_TEXT_SIZE, ORANGE, START_FONT, centered=True)
        self.draw_text('1 PLAYER ONLY', self.screen, (WIDTH//2, HEIGHT//2 + 30), START_TEXT_SIZE, BLUE, START_FONT, centered=True)
        self.draw_text('BY FRaverta', self.screen, (WIDTH // 2, HEIGHT // 2 + 100), START_TEXT_SIZE, WHITE, START_FONT, centered=True)
        pygame.display.update()

    ###########################################################
    #################### Playing functions ####################
    ###########################################################

    def playing_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    self.player.move(vec(-1, 0))
                if event.key == pygame.K_RIGHT:
                    self.player.move(vec(1, 0))
                if event.key == pygame.K_UP:
                    self.player.move(vec(0, -1))
                if event.key == pygame.K_DOWN:
                    self.player.move(vec(0, 1))

    def playing_update(self):
        self.player.update()
        for e in self.enemies:
            e.update()

        for e in self.enemies:
            if self.player.grid_pos == e.grid_pos:
                self.player.remove_life()
                if self.player.lifes == 0:
                    print("GAME OVER")
                    self.state = GAME_OVER

                #else:
                #for e in self.enemies:
                sleep(2)
                for e in self.enemies:
                    e.restart()
                break

        #print(len(self.coins))
        if (len(self.coins) == 0):
            self.state = GAME_WIN


    def playing_draw(self):
        self.screen.fill(BLACK)
        self.draw_text(f'CURRENT SCORE {self.player.score}', self.screen, (MARGIN_LEFT,15), 16, WHITE, 'arial black')
        self.draw_text('HIGH SCORE 0', self.screen, (MARGIN_LEFT + MAZE_WIDTH//2, 15), 16, WHITE, 'arial black')
        self.screen.blit(self.background, (MARGIN_LEFT, MARGIN_TOP))
        #self.draw_grid()
        self.draw_coins()
        self.draw_fruits()
        self.player.draw()
        for e in self.enemies:
            e.draw()
        pygame.display.update()


    def draw_coins(self):
        for x,y in self.coins:
            pygame.draw.circle(self.screen, COINS_COLOR,
                             (self.maze_x((x + 0.5) * self.cell_width), self.maze_y((y + 0.5) * self.cell_height)), 4)

    def draw_fruits(self):
        if len(self.fruits) == 0:
            if self.fruit_clock == 0:
                self.fruits.append((9, 11))
                self.fruit_clock = 500
        else:
            if self.fruit_clock == 0:
                self.fruits = []
                self.fruit_clock = 500

        self.fruit_clock -= 1


        for x,y in self.fruits:
            fruit = pygame.image.load('resources/cherry.png')
            width_proportion = fruit.get_width() / fruit.get_height()
            height_proportion = fruit.get_height() / fruit.get_width()
            fruit = pygame.transform.scale(fruit, (int(width_proportion * self.cell_width * 0.95), int(height_proportion * self.cell_height * 0.95)))
            self.screen.blit(fruit, (self.maze_x((x + 0.2) * self.cell_width), self.maze_y((y + 0.1) * self.cell_height)))

    ###########################################################
    #################### Game over functions ##################
    ###########################################################

    def game_over_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.state = START



    def game_over_update(self):
        self.screen.fill(BLACK)
        self.draw_text(f'CURRENT SCORE {self.player.score}', self.screen, (MARGIN_LEFT, 15), 16, WHITE, 'arial black')
        self.draw_text('HIGH SCORE 0', self.screen, (MARGIN_LEFT + MAZE_WIDTH // 2, 15), 16, WHITE, 'arial black')
        self.draw_text('GAME OVER', self.screen, (WIDTH // 2, HEIGHT // 2), 50, RED, START_FONT,
                       centered=True)


        pygame.display.update()

    def game_over_draw(self):
        pass


    ###########################################################
    #################### Game Win  functions #######################
    ###########################################################

    def game_win_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    self.restart_game(game_over=False)
                    self.state = PLAYING


    def game_win_update(self):
        self.screen.fill(BLACK)
        self.draw_text(f'CURRENT SCORE {self.player.score}', self.screen, (MARGIN_LEFT, 15), 16, WHITE, 'arial black')
        self.draw_text('HIGH SCORE 0', self.screen, (MARGIN_LEFT + MAZE_WIDTH // 2, 15), 16, WHITE, 'arial black')
        self.draw_text('Win', self.screen, (WIDTH // 2, HEIGHT // 2), 50, GREEN, START_FONT,
                       centered=True)
        self.draw_text('PUSH SPACE BAR TO CONTINUE', self.screen, (WIDTH // 2, (HEIGHT + 100) // 2), 25, ORANGE, START_FONT, centered=True)


        pygame.display.update()


    def game_win_draw(self):
        pass